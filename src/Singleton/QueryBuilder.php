<?php

namespace PawelSpychalski\DesignPatterns\Singleton;

class QueryBuilder
{
    private static QueryBuilder $instance;

    private function __construct()
    {
    }

    public static function query(): QueryBuilder
    {
        if(empty(self::$instance)) {
            self::$instance = new QueryBuilder();
        }

        return self::$instance;
    }
}
