<?php

namespace PawelSpychalski\DesignPatterns\Singleton;

use PHPUnit\Framework\TestCase;

class QueryBuilderTest extends TestCase
{
    public function testGettingTheInstance(): void
    {
        $queryBuilder = QueryBuilder::query();

        $this->assertInstanceOf(QueryBuilder::class, $queryBuilder);
    }

    public function testSingletonGivesOnlyOneInstance(): void
    {
        $queryBuilder = QueryBuilder::query();

        $this->assertInstanceOf(QueryBuilder::class, QueryBuilder::query());
        $this->assertEquals($queryBuilder, QueryBuilder::query());
        $this->assertSame($queryBuilder, QueryBuilder::query());
    }
}
