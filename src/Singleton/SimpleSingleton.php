<?php

namespace PawelSpychalski\DesignPatterns\Singleton;

/**
 * The simplest form of Singleton class
 */
class SimpleSingleton
{
    private static SimpleSingleton $instance;

    private function __construct()
    {
    }

    public static function instance(): SimpleSingleton
    {
        if (empty(self::$instance)) {
            self::$instance = new SimpleSingleton();
        }

        return self::$instance;
    }
}
