<?php

namespace PawelSpychalski\DesignPatterns\Singleton;

use PHPUnit\Framework\TestCase;

class SimpleSingletonTest extends TestCase
{
    public function testTryingCreateInstanceInPublicError(): void
    {
        $this->expectError();

        $singleton = new SimpleSingleton();
    }
}
